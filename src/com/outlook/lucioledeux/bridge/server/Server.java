package com.outlook.lucioledeux.bridge.server;

import com.outlook.lucioledeux.bridge.exception.ServerStopException;

import java.io.IOException;
import java.net.ServerSocket;

/**
 * This interface includes the basic elements needed to
 * create a Server class that would still be compatible
 * with the Bridge software.
 */
public interface Server {

    boolean getIsRunning();
    int getServerPort();
    ServerSocket getServerSocket();

    void setIsRunning(boolean running);
    void setServerPort(int port);
    void setServerSocket(ServerSocket socket);

    void start();
    default void stop() throws ServerStopException {
        setIsRunning(false);

        try {
            getServerSocket().close();
        } catch (Exception ex) {
            throw new ServerStopException(ex.getMessage());
        }
    }
}
