package com.outlook.lucioledeux.bridge.server;

import com.outlook.lucioledeux.bridge.protocol.BridgeClientResponseType;

import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public abstract class BridgeClientManager {

    private Map<String, BridgeClient> clients = new ConcurrentHashMap<String, BridgeClient>();

    public void addClient(BridgeClient client) {
        clients.put(client.getUsername(), client);
    }

    public Collection<BridgeClient> getClients() {
        return clients.values();
    }

    public BridgeClient getClient(String username) {
        return clients.get(username);
    }

    public abstract void onDisconnect();
    public abstract void onGetRequest(BridgeClientResponseType type, String... args);
    public abstract void onWelcome();
}
