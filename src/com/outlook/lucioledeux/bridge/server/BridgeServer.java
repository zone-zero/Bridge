package com.outlook.lucioledeux.bridge.server;

import com.outlook.lucioledeux.bridge.exception.ServerStopException;
import com.outlook.lucioledeux.bridge.protocol.BridgeClientResponseType;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public abstract class BridgeServer implements Server, Runnable {

    private BridgeClientManager clientManager;
    private boolean isRunning = false;
    private int port = 9999;
    private ServerSocket socket;
    private Thread server;

    public BridgeServer() {
        clientManager = new BridgeClientManager() {

            @Override
            public void onDisconnect() {
                onUserDisconnect();
            }

            @Override
            public void onGetRequest(BridgeClientResponseType type, String... args) {
                onUserRequestReceived(type, args);
            }

            @Override
            public void onWelcome() {
                onUserJoin();
            }
        };
    }

    @Override
    public boolean getIsRunning() {
        return isRunning;
    }

    @Override
    public int getServerPort() {
        return port;
    }

    @Override
    public ServerSocket getServerSocket() {
        return socket;
    }

    public abstract void onUserDisconnect();
    public abstract void onUserJoin();
    public abstract void onUserRequestReceived(BridgeClientResponseType type, String... args);

    @Override
    public void run() {
        synchronized (this) {
            server = Thread.currentThread();
        }

        start();

        while (isRunning) {
            Socket client = null;

            try {
                client = socket.accept();
            } catch (IOException ex) {
                if (!isRunning) {

                }
            }

            new Thread(new BridgeClient(client)).start();
        }
    }

    @Override
    public void setIsRunning(boolean running) {
        this.isRunning = running;
    }

    @Override
    public void setServerPort(int port) {
        this.port = port;
    }

    @Override
    public void setServerSocket(ServerSocket socket) {
        this.socket = socket;
    }

    @Override
    public void start() {
        try {
            this.socket = new ServerSocket(this.port);
        } catch (Exception ex) {
            // TODO Exception
        }
    }

    @Override
    public void stop() throws ServerStopException {
        setIsRunning(false);
        try {
            socket.close();
        } catch (IOException ex) {
            // TODO Exception
        }
    }
}
