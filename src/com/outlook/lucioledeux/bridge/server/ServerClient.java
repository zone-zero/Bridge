package com.outlook.lucioledeux.bridge.server;

import com.outlook.lucioledeux.bridge.exception.UserConnectFailureException;
import com.outlook.lucioledeux.bridge.exception.UsernameExistsException;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

public interface ServerClient extends Runnable {

    int getClientId();
    InputStream getInputStream();
    OutputStream getOutputStream();
    String getServerAddress();
    Socket getSocket();
    String getUsername();
    void onJoin();

    /**
     * Sends a Welcome to the user, indicating that the user should send
     * details to the server.
     * @throws UsernameExistsException
     * @throws UserConnectFailureException
     */
    void sendWelcome() throws UsernameExistsException, UserConnectFailureException;

}
