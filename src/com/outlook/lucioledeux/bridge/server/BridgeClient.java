package com.outlook.lucioledeux.bridge.server;

import com.outlook.lucioledeux.bridge.exception.UserConnectFailureException;
import com.outlook.lucioledeux.bridge.exception.UsernameExistsException;
import com.outlook.lucioledeux.bridge.protocol.BridgeServerRequestType;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

public class BridgeClient implements ServerClient {

    private Socket client;
    private BufferedReader in;
    private InputStream input;
    private BufferedWriter out;
    private OutputStream output;
    private Scanner scanner;
    private int transaction = -1;
    private String username;

    public BridgeClient(Socket socket) {
        try {
            client = socket;
            input = client.getInputStream();
            output = client.getOutputStream();
            in = new BufferedReader(new InputStreamReader(input));
            out = new BufferedWriter(new OutputStreamWriter(output));
        } catch (IOException ex) {
            System.err.println("An error occurred while initializing a client: " + ex.getMessage());
        }
    }

    public void disconnect() {
        send(BridgeServerRequestType.BYE, String.valueOf(getCurrentTransaction()));
    }

    public int getCurrentTransaction() {
        transaction += 1;
        return transaction;
    }

    @Override
    public int getClientId() {
        return 0;
    }

    @Override
    public InputStream getInputStream() {
        return input;
    }

    @Override
    public OutputStream getOutputStream() {
        return output;
    }

    @Override
    public String getServerAddress() {
        return null;
    }

    @Override
    public Socket getSocket() {
        return client;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public void onJoin() {
        send(BridgeServerRequestType.PWD); // Sends password request to user. Used to begin authentication.

    }

    public void send(BridgeServerRequestType type) {
        try {
            String outputMessage = type.name() + " " + String.valueOf(getCurrentTransaction());
            out.write(outputMessage);
            out.flush();
        } catch (IOException ex) {

        }
    }

    public void send(BridgeServerRequestType type, String... args) {
        try {
            StringBuilder outputMessageBuilder = new StringBuilder();
            outputMessageBuilder.append(type.name());
            stamp(outputMessageBuilder);
            String outputMessage = type.name() + " " + String.valueOf(getCurrentTransaction());
            for (String x : args) {
                stamp(outputMessageBuilder, x);
            }
            out.write(outputMessageBuilder.toString());
            out.flush();
        } catch (IOException ex) {

        }
    }

    private void stamp(StringBuilder builder) {
        builder.append(" ");
        builder.append(getCurrentTransaction());
    }

    private void stamp(StringBuilder builder, String arg) {
        builder.append(" ");
        builder.append(arg);
    }

    @Override
    public void sendWelcome() throws UsernameExistsException, UserConnectFailureException {
        send(BridgeServerRequestType.WEL);
    }

    @Override
    public void run() {
        try {
            in.read();
            // TODO Handle input of client messaging
        } catch (IOException ex) {

        }
    }
}
