package com.outlook.lucioledeux.bridge.protocol;

public enum BridgeClientResponseType {

    /**
     * This acts as the disconnect command. Is typically sent last.
     */
    BYE,

    /**
     * Acts as a broadcast message. This sends a message to the server,
     * which then would send this message to all clients.
     */
    BDT,

    /**
     * Acts as the conversation request command. Used in order to
     * respond to a CAL command from the server.
     */
    CAL,

    /**
     * Acts as the primary command for all commands. Allows for
     * extensibility between the client and the server.
     */
    CMD,

    /**
     * Acts as the client version information command. Sent after
     * the client receives the SVR command.
     */
    CVR,

    /**
     * Acts as a response to the server's "EXT" command. Used only
     * to respond that the server has received the extended command.
     * Also used to respond to a server's "NUM" command.
     */
    EXT,

    /**
     * This acts as the disconnect request command. This would trigger a
     * "BYE" response from the Server.
     */
    OUT,

    /**
     * This acts as the message command in order to send a command to
     * a specific user.
     */
    MSG,

    /**
     * This acts as the password command, which is required for
     * users who wish to connect to private servers. This command should
     * be the first encrypted command, and the first command, as well as
     * the command before the user receives a WEL command.
     */
    PWD,
}
