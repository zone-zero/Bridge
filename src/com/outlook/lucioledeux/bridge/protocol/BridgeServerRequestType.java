package com.outlook.lucioledeux.bridge.protocol;

public enum BridgeServerRequestType {

    /**
     * Acts as the "Disconnect" command, alerting the client that
     * the Server is sending a disconnect notification to the
     * user.
     *
     * The following disconnect types are available:
     *  - A = Uncontrolled disconnect, possible server error.
     *  - B = Controlled disconnect, requested by server administrator.
     *  - D = Controlled disconnect, requested by user.
     *  - F = Uncontrolled disconnect, possible server failure.
     *  - K = Controlled disconnect, requested by server administrator.
     *  - O = Uncontrolled disconnect, server older version than client.
     *  - U = Uncontrolled disconnect, update required on client end.
     * {@code BYE 20 DisconnectType}
     */
    BYE,

    /**
     * Sends a request to begin a conversation to a user who has
     * been requested.
     * {@code CAL 21 PartnerUsernameBytes}
     */
    CAL,

    /**
     * Sends command established by server to users. Used for allowing
     * custom commands to be parsed by the user, but requires interaction
     * with the user's client.
     *
     * This command would only work if the client's extensibility matches
     * the server's extensibility.
     */
    CMD,

    /**
     * Sends command established by functionality to all users. This allows
     * communication between clients for features such as imaging used or
     * background music.
     *
     * Stands for extended command.
     *
     * This command only works if the client matches the server in terms of
     * program information.
     */
    EXT,

    /**
     * Sends a message to a user.
     * {@code MSG 22 Sender MessageBytes}
     */
    MSG,

    /**
     * Sends numeric information to all users. This allows communication
     * between clients for numbers such as scores or those who are
     * currently online.
     *
     * Stands for numeric extended command.
     */
    NUM,

    /**
     * Acts as a password request command. Acts as the first command
     * that the user receives before entering the server.
     */
    PWD,

    /**
     * Server version check with Client. Sends server version
     * information to client, including Protocol version.
     *
     * In response, a client should send its client version ("CVR")
     * information back to the server, including program name and
     * program version.
     */
    SVR,

    /**
     * Server list update, sending a list of users that are
     * connected.
     */
    SLU,

    /**
     * Acts as a "Welcome" command, alerting the user that the
     * Server is requesting the client for identification.
     */
    WEL
}
