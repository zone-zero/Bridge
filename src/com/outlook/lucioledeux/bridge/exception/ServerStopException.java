package com.outlook.lucioledeux.bridge.exception;

public class ServerStopException extends Exception {

    public ServerStopException(String message) {
        super(message);
    }

}
