package com.outlook.lucioledeux.bridge.exception;

public class UserConnectFailureException extends Exception {

    public UserConnectFailureException(String message) {
        super(message);
    }

}
