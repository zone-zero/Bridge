package com.outlook.lucioledeux.bridge.exception;

public class ClientRequestSendException extends Exception {

    public ClientRequestSendException(String message) {
        super(message);
    }

}
