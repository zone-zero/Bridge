package com.outlook.lucioledeux.bridge.exception;

public class UsernameExistsException extends Exception {

    public UsernameExistsException(String message) {
        super(message);
    }

}
